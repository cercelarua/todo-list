### Tech Stack 

- React (SSR)
- Typescript
- Graphql ( Apollo server and client )
- Socket.io ( for real-time data syncing )
- ExpressJS
- Lerna monorepo
- Webpack
- ESLint

### Running the project

- Clone this repo  `$ git clone https://cercelarua@bitbucket.org/cercelarua/todo-list.git`

-  Run  `$ yarn install && yarn dev` in the cloned repo dir

The `yarn dev` command should build all the packages using webpack and bring up the  Express ( for SSR ) and GraphQL ( API for todos operations ) servers ( could take ~30s )

-    Please point your browser to `http://localhost:3000`

### Hosts
- Express Server : `http://localhost:3000`
- Apollo Server :   `http://localhost:4000`


### Packages
- `@todo-list/client`
- `@todo-list/api`
- `@todo-list/page-renderer`
- `@todo-list/shared-app`
