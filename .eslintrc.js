module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint',
    'plugin:react/recommended'
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'prettier'],
  parserOptions: {
    ecmaVersion: 7,
    sourceType: 'module',
  },
  rules: {
    '@typescript-eslint/indent': 0,
    '@typescript-eslint/camelcase': 0,
    '@typescript-eslint/no-unused-vars': [2, { args: 'none' }],
    'prettier/prettier': [
      'error',
      { singleQuote: true, trailingComma: 'all', semi: false },
    ],
    '@typescript-eslint/ban-ts-ignore': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/explicit-function-return-type': 0,
    '@typescript-eslint/no-var-requires': 0,
    'no-console': 0,
    'no-return-await': 'error',
    'no-duplicate-imports': 'error',
    '@typescript-eslint/explicit-module-boundary-types': 0
  },
  env: {
    browser: true,
    node: true,
    jest: true,
    es6: true,
  },
   settings: {
     react: {
        version: "detect"
     }
   }
}
