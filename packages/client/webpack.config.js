const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: './index.ts',
    devtool: 'source-map',
    resolve: {
      extensions: [ '.js', '.jsx', '.json', '.ts', '.tsx' ]
    },
    output: {   
      path: path.resolve('../public'),
      filename: 'client.js',
      publicPath: '/assets'
    },
    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'ts-loader',
                    }
                ]
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            hash: true,
            template: './src/index.html',
            title: 'Todos App',
            filename: './index.html'
        })
   ]
}