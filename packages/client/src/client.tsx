import React from 'react'
import { hydrate, render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { App } from '@todo-list/shared-app'
import fetch from 'cross-fetch'
import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
} from '@apollo/client'
import serverConfig from '../../server/page-renderer/serverConfig'

const { APOLLO_SERVER_HOST } = serverConfig

const apolloClient = new ApolloClient({
  link: new HttpLink({ uri: APOLLO_SERVER_HOST, fetch }),
  cache: new InMemoryCache(),
})

const root = document.getElementById('root')
let renderMethod
if (root && root.innerHTML !== '') {
  renderMethod = hydrate
} else {
  renderMethod = render
}

renderMethod(
  <ApolloProvider client={apolloClient}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </ApolloProvider>,
  document.getElementById('root'),
)
