import { ApolloServer } from 'apollo-server-express'
import { default as todos } from './todos'
import express from 'express'
import serverConfig from '../page-renderer/serverConfig'

const { APOLLO_SERVER_PORT } = serverConfig

const { typeDefs, resolvers } = todos

const server = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: true,
  playground: true,
})

const app = express()
server.applyMiddleware({ app })

app.listen({ port: APOLLO_SERVER_PORT }, () => {
  console.log(`Apollo server listening on port ${APOLLO_SERVER_PORT}`)
})
