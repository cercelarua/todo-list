import { gql } from 'apollo-server'

const typeDefs = gql`
  type Query {
    GetAllToDos: GetAllToDosResponse!
  }

  type ToDo {
    id: ID!
    text: String!
    completed: Boolean
  }

  input CreateToDoInput {
    text: String!
  }

  input DeleteToDoInput {
    id: ID!
  }

  type CreateToDoResponse {
    todos: [ToDo]!
  }

  type DeleteToDoResponse {
    todos: [ToDo]!
  }

  input UpdateToDoInput {
    id: ID!
    completed: Boolean
  }

  type UpdateToDoResponse {
    todo: ToDo!
  }

  type GetAllToDosResponse {
    todos: [ToDo]!
  }

  type Mutation {
    CreateToDo(input: CreateToDoInput!): CreateToDoResponse!
    DeleteToDo(input: DeleteToDoInput!): DeleteToDoResponse!
    UpdateToDo(input: UpdateToDoInput!): UpdateToDoResponse!
  }
`

export default typeDefs
