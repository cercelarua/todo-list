import { JsonDB } from 'node-json-db'
import { Config } from 'node-json-db/dist/lib/JsonDBConfig'

const DB_NAMES = 'todos'

const db = new JsonDB(new Config(DB_NAMES, true, false, '/'))

export default db
