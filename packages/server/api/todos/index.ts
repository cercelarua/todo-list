import { default as typeDefs } from './typeDefs'
import {
  createToDoResolver,
  getAllToDosResolver,
  deleteToDoResolver,
  updateToDoResolver,
} from './resolvers'

export default {
  typeDefs,
  resolvers: {
    Mutation: {
      CreateToDo: createToDoResolver,
      DeleteToDo: deleteToDoResolver,
      UpdateToDo: updateToDoResolver,
    },
    Query: {
      GetAllToDos: getAllToDosResolver,
    },
  },
}
