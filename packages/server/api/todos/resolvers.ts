import db from './db/db'
import { v4 as uuidv4 } from 'uuid'

type CreateToDoInput = {
  input: {
    text: string
  }
}

type DeleteToDoInput = {
  input: {
    id: string
  }
}

type UpdateToDoInput = {
  input: {
    id: string
    completed: boolean
  }
}

type ToDo = {
  id: string
  text: string
  completed: boolean
}

export type CreateToDoResponse = {
  todos: ToDo[]
}

export type DeleteToDoResponse = {
  todos: ToDo[]
}

export type GetAllToDosResponse = {
  todos: ToDo[]
}

export type UpdateToDoResponse = {
  todo: ToDo
}

export const createToDoResolver = (
  _: null,
  args: CreateToDoInput,
  ___: null,
): CreateToDoResponse => {
  const id = uuidv4()
  db.push(`/${id}`, {
    text: args.input.text,
    completed: false,
    id,
  })

  const todosInDb = db.getData('/')
  const allToDos = Object.keys(todosInDb).map((key) => todosInDb[key])

  return {
    todos: allToDos,
  }
}

export const getAllToDosResolver = (
  _: null,
  __: null,
  ___: null,
): GetAllToDosResponse => {
  const todosInDb = db.getData('/')
  const allToDos = Object.keys(todosInDb).map((key) => todosInDb[key])

  return {
    todos: allToDos,
  }
}

export const deleteToDoResolver = (
  _: null,
  args: DeleteToDoInput,
  ___: null,
): DeleteToDoResponse => {
  const idToDelete = args.input.id
  db.delete(`/${idToDelete}`)

  const todosInDb = db.getData('/')
  const allToDos = Object.keys(todosInDb).map((key) => todosInDb[key])

  return {
    todos: allToDos,
  }
}

export const updateToDoResolver = (
  _: null,
  args: UpdateToDoInput,
  ___: null,
): UpdateToDoResponse => {
  const idToUpdate = args.input.id

  const todoToUpdate = db.getData(`/${idToUpdate}`) as ToDo

  const updatedToDo = {
    id: todoToUpdate.id,
    text: todoToUpdate.text,
    completed: args.input.completed,
  }

  db.push(`/${idToUpdate}`, updatedToDo)

  return {
    todo: updatedToDo,
  }
}
