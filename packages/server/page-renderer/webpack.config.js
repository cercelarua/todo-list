const path = require('path')
const NodemonPlugin = require('nodemon-webpack-plugin'); 

const webpackConfig = {
    mode: 'development',
    entry: './index.ts', 
    devtool: 'source-map',
    resolve: {
      extensions: [ '.js', '.jsx', '.json', '.ts', '.tsx' ]
    },
    output: {
      libraryTarget: 'commonjs2',
      path: path.join(__dirname, '.build'),
      filename: 'server.js'
    },
    target: 'node',
    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'ts-loader',
                    }
                ]
            },
        ]
    },
    optimization: {
        minimize: false
    },
    plugins: [
        new NodemonPlugin()
    ],
}

module.exports = webpackConfig