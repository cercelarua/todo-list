import express, { Request, Response } from 'express'
import ReactDOMServer from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import React from 'react'
import fs from 'fs'
import path from 'path'
import util from 'util'
import { App } from '@todo-list/shared-app'
import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
} from '@apollo/client'
import fetch from 'cross-fetch'
import * as socketIo from 'socket.io'
import serverConfig from '../serverConfig'

const app = express()

const ASSETS_PATH = '../../public'
const { APOLLO_SERVER_HOST, EXPRESS_SERVER_PORT } = serverConfig

const createApolloClient = () => {
  return new ApolloClient({
    ssrMode: true,
    link: new HttpLink({ uri: APOLLO_SERVER_HOST, fetch }),
    cache: new InMemoryCache(),
  })
}

const readIndexFile = async (): Promise<string> => {
  const readFile = util.promisify(fs.readFile)
  const indexPath = path.resolve(`${ASSETS_PATH}/index.html`)
  try {
    return readFile(indexPath, 'utf8')
  } catch (e) {
    const errorMessage = 'Could not read index file'
    console.error(errorMessage, { indexPath })
    throw new Error(errorMessage)
  }
}

app.use('/assets', express.static(ASSETS_PATH))

app.get('*', async (request: Request, response: Response) => {
  const context = {}
  const appHtml = ReactDOMServer.renderToString(
    <ApolloProvider client={createApolloClient()}>
      <StaticRouter location={request.url} context={context}>
        <App />
      </StaticRouter>
    </ApolloProvider>,
  )

  try {
    const indexFile = await readIndexFile()
    return response.send(
      indexFile.replace(
        '<div id="root"></div>',
        `<div id="root">${appHtml}</div>`,
      ),
    )
  } catch (e) {
    return response.status(500).send(e)
  }
})

const server = app.listen(EXPRESS_SERVER_PORT, () => {
  console.log(`Express server is listening on port ${EXPRESS_SERVER_PORT}`)
})

const io = socketIo.listen(server, { serveClient: false })

io.on('connection', (client: any) => {
  console.log('New user has connected ...')
  client.on('toDoListChange', () => {
    io.emit('refetchToDos')
  })
})
