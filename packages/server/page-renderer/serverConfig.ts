const EXPRESS_SERVER_PORT = process.env.EXPRESS_PORT || 3000
const APOLLO_SERVER_PORT = process.env.APOLLO_PORT || 4000

export default {
  APOLLO_SERVER_HOST: `http://localhost:${APOLLO_SERVER_PORT}/graphql`,
  EXPRESS_SERVER_PORT,
  APOLLO_SERVER_PORT,
}
