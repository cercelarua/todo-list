import React from 'react'
import { Route, Switch } from 'react-router-dom'
import TodosPage from './pages/todos-page/components/TodosPage'

const App = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={TodosPage} />
      </Switch>
    </div>
  )
}

export default App
