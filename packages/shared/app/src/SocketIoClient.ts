import client from 'socket.io-client'
const socketIoClient: SocketIOClient.Socket = client()

export default socketIoClient
