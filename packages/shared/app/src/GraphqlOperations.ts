import { gql } from '@apollo/client'

export const addToDoMutation = gql`
  mutation CreateToDo($text: String!) {
    CreateToDo(input: { text: $text }) {
      todos {
        id
        text
        completed
      }
    }
  }
`

export const getAllToDosQuery = gql`
  query GetAllToDos {
    GetAllToDos {
      todos {
        id
        text
        completed
      }
    }
  }
`

export const deleteToDoMutation = gql`
  mutation DeleteToDo($id: ID!) {
    DeleteToDo(input: { id: $id }) {
      todos {
        id
        text
        completed
      }
    }
  }
`

export const updateToDoMutation = gql`
  mutation UpdateToDo($id: ID!, $completed: Boolean) {
    UpdateToDo(input: { id: $id, completed: $completed }) {
      todo {
        id
        text
        completed
      }
    }
  }
`

export type GetAllToDosResponseData = {
  todos: {
    id: string
    text: string
    completed: boolean
  }[]
}
