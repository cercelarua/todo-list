import styled from '@emotion/styled'
import React, { useEffect } from 'react'
import ToDoItem from '../components/TodoItem'
import { useMutation, useQuery } from '@apollo/client'
import {
  deleteToDoMutation,
  getAllToDosQuery,
  updateToDoMutation,
  GetAllToDosResponseData,
} from '../../../GraphqlOperations'
import socketIoClient from '../../../SocketIoClient'

const TodoListWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-top: 30px;
  font-family: Arial;
  font-size: 14px;
`

const TodoListHeader = styled.div`
  text-indent: 25px;
  height: 50px;
  display: flex;
  align-items: center;
  border: 1px solid lightgray;
  flex-grow: 1;
`

const TodoListItems = styled.div`
  border: 1px solid lightgray;
  border-top: 0;
  padding: 30px;
  > div:not(:last-child) {
    border-bottom: 1px solid lightgray;
  }
  max-height: 500px;
  overflow: auto;
`

const ToDoItemsPlaceholder = styled.div`
  font-style: italic;
  font-size: 12px;
  color: #7a7c80;
`

const TodosList = () => {
  const [deleteToDo] = useMutation(deleteToDoMutation)
  const [updateToDo] = useMutation(updateToDoMutation)

  const onDeleteToDo = (id: string) => {
    deleteToDo({ variables: { id }, refetchQueries: ['GetAllToDos'] })
    socketIoClient.emit('toDoListChange')
  }

  const onUpdateToDo = (id: string, completed: boolean) => {
    updateToDo({
      variables: { id, completed },
      refetchQueries: ['GetAllToDos'],
    })
    socketIoClient.emit('toDoListChange')
  }

  const { loading, error, data, refetch: refetchToDos } = useQuery(
    getAllToDosQuery,
    {
      fetchPolicy: 'no-cache',
    },
  )

  useEffect(() => {
    socketIoClient.on('refetchToDos', refetchToDos)
  })

  if (loading) return <p>Loading...</p>
  if (error) return <p>Error encountered</p>

  const todos = (data.GetAllToDos as GetAllToDosResponseData).todos
  return (
    <TodoListWrapper>
      <TodoListHeader>Todo List</TodoListHeader>
      <TodoListItems>
        {!todos.length ? (
          <ToDoItemsPlaceholder>
            Please add something which needs to be done ...
          </ToDoItemsPlaceholder>
        ) : (
          todos.map((todo) => (
            <ToDoItem
              key={todo.id}
              todo={todo}
              onDeleteToDo={() => onDeleteToDo(todo.id)}
              onUpdateToDo={() => onUpdateToDo(todo.id, !todo.completed)}
            />
          ))
        )}
      </TodoListItems>
    </TodoListWrapper>
  )
}

export default TodosList
