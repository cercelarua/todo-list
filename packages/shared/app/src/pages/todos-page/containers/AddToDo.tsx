import React from 'react'
import AddToDoForm from '../components/AddToDoForm'
import { addToDoMutation } from '../../../GraphqlOperations'
import { useMutation } from '@apollo/client'
import socketIoClient from '../../../SocketIoClient'

const AddToDo = () => {
  const [addTodo] = useMutation(addToDoMutation)
  const onSubmit = (toDoText: string): void => {
    addTodo({ variables: { text: toDoText }, refetchQueries: ['GetAllToDos'] })
    socketIoClient.emit('toDoListChange')
  }

  return (
    <>
      <AddToDoForm onSubmit={onSubmit} />
    </>
  )
}

export default AddToDo
