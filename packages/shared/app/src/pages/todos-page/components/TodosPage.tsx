import React from 'react'
import AddToDo from '../containers/AddToDo'
import styled from '@emotion/styled'
import TodosList from '../containers/TodosList'

const PageWrapper = styled.div`
  max-width: 900px;
  height: 100%;
  margin: 0 auto;
  position: relative;
  padding: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const ToDosPage = () => (
  <PageWrapper>
    <AddToDo />
    <TodosList />
  </PageWrapper>
)

export default ToDosPage
