import styled from '@emotion/styled'
import React, { ChangeEvent, FormEvent, useState } from 'react'
import { PlusCircleFilled } from '@ant-design/icons'

const AddToDoInput = styled.input`
  height: 83%;
  flex-grow: 1;
  margin-right: 30px;
  text-indent: 10px;
`
const AddToDoSubmitButton = styled.button`
  height: 100%;
  flex-grow: 1;
  max-width: 250px;
  background-color: #5bafe7;
  border: none;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`

const StyledForm = styled.form`
  width: 100%;
  height: 30px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const AddLabel = styled.span`
  margin-left: 5px;
`

type Props = {
  onSubmit: (toDoText: string) => void
}

const AddToDoForm = ({ onSubmit }: Props) => {
  const [toDoText, setToDoText] = useState('')

  return (
    <StyledForm
      onSubmit={(e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (!toDoText.trim()) {
          return
        }

        setToDoText('')
        onSubmit(toDoText)
      }}
    >
      <AddToDoInput
        type="text"
        value={toDoText}
        placeholder="What needs to be done ?"
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          setToDoText(e.target.value)
        }
      />
      <AddToDoSubmitButton type="submit">
        <PlusCircleFilled />
        <AddLabel>Add</AddLabel>
      </AddToDoSubmitButton>
    </StyledForm>
  )
}

export default AddToDoForm
