import styled from '@emotion/styled'
import React from 'react'
import { DeleteTwoTone } from '@ant-design/icons'

type ToDoItemProps = {
  todo: {
    completed: boolean
    text: string
    id: string
  }
  onDeleteToDo: () => void
  onUpdateToDo: () => void
}

const DeleteToDoButton = styled.button`
  cursor: pointer;
  border: 1px solid lightgray;
  width: 30px;
  height: 30px;
`

const StyledToDoItem = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  height: 40px;
  align-items: center;
  font-size: 14px;
  color: #7a7c80;
`

const ToDoLabel = styled.span`
  margin-left: 15px;
  text-decoration: ${(props: { completed: boolean }) =>
    props.completed ? 'line-through' : 'none'};
`

const ToDoItem = ({
  onDeleteToDo,
  onUpdateToDo,
  todo: { completed, text, id },
}: ToDoItemProps) => (
  <StyledToDoItem key={id}>
    <div>
      <input type="checkbox" onChange={onUpdateToDo} checked={completed} />
      <ToDoLabel completed={completed}>{text}</ToDoLabel>
    </div>
    <DeleteToDoButton onClick={onDeleteToDo}>
      <DeleteTwoTone twoToneColor="red" />
    </DeleteToDoButton>
  </StyledToDoItem>
)

export default ToDoItem
